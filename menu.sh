#!/bin/bash

printmenu(){
  echo File: $file
  echo "v) View file"
  echo "e) Edit file"
  echo "c) Compare file"
  echo "q) Quit"
}

for file in *; do
  printmenu
  read reply
  if [[ $reply = "v" ]]; then
    less $file
  elif [[ $reply = "e" ]]; then
    jpico $file
  elif [[ $reply = "c" ]]; then
    g++ $file
  elif [[ $reply = "q" ]]; then
    exit
  else
    echo "Invalid response, skipping file" 
  fi
done
  
