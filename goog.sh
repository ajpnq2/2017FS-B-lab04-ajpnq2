#!/bin/bash

if [[ $# = 2 ]]; then
  echo $2: $(wget --quiet -O - $1|grep -o $2|wc -l)

else
  echo "Use goog.sh with 2 arguments in the form: goog.sh [URL] [WORD]"
  
fi
